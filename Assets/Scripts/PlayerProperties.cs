﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class PlayerProperties : MonoBehaviour
{
    public string PlayerName;
    public int NoOfCards;
    //public int Score;
    public bool isTurn;
    public GameObject activeCard;
    public Transform ShowCardPos;
    public Transform WarCardPos;
    public Transform CardFlipPos;
    public TextMeshProUGUI cardsText;
    public Queue<GameObject> MyStack = new Queue<GameObject>();
}
