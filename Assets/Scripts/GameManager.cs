﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    public static GameManager Instance;


    public GameObject cardPrefab;
    public int totalCards;

    public GameObject player1SpawnPos;
    public GameObject player2SpawnPos;

    public Transform Player1CardsParent;
    public Transform Player2CardsParent;

    public Queue<GameObject> FlipedCards = new Queue<GameObject>();
    public List<GameObject> CardsOnTable = new List<GameObject>();

    public enum Mode
    {
        None,
        Battle,
        War
    }

    public Mode currentMode;
    public Mode previousMode;

    public Text GameOverText;

    private Vector3 spawnX1Increment = new Vector3(0.2f, 0, 0);
    private Vector3 spawnX2Increment = new Vector3(-0.2f, 0, 0);


    private void Awake()
    {
        Instance = this;
    }

    private void Update()
    {

        if (CardsOnTable.Count == 2)
        {
            CheckForGameMode();
        }
        else
        {
            currentMode = Mode.None;
        }
    }

    private void Start()
    {
        currentMode = Mode.None;

        InitializeGame();
    }

    void InitializeGame()
    {
        for(int i =0; i < totalCards; i++)
        {
            if(i%2==0)
            {                
                GameObject cards = Instantiate(cardPrefab, player1SpawnPos.transform.position, Quaternion.identity, Player1CardsParent.transform);

                AddToPlayer1CardStack(cards);                

                SetCardNumber(cards);
                //cards.transform.position = player1SpawnPos.transform.position + spawnX1Increment;
                //spawnX1Increment.x = spawnX1Increment.x + 0.2f;
            }
            else
            {                
                GameObject cards = Instantiate(cardPrefab, player2SpawnPos.transform.position, Quaternion.identity, Player2CardsParent.transform);

                AddToPlayer2CardStack(cards);

                SetCardNumber(cards);
                //cards.transform.position = player2SpawnPos.transform.position + spawnX2Increment;
                //spawnX2Increment.x = spawnX2Increment.x - 0.2f;
            }                
        }
    }

    void CheckForGameMode()
    {
        if (CardsOnTable[0].GetComponent<Cards>().cardNumber == CardsOnTable[1].GetComponent<Cards>().cardNumber)
        {
            currentMode = Mode.War;
        }
        else
        {
            currentMode = Mode.Battle;
        }
    }

    void SetCardNumber(GameObject instance)
    {
        int num = Random.Range(2, 6);

        instance.GetComponent<Cards>().cardNumber = num;
        instance.GetComponent<Cards>().numberText.text = num.ToString();
        instance.GetComponent<Cards>().numberText.enabled = false;
    }

    public void AddToPlayer1CardStack(GameObject card)
    {
        player1SpawnPos.GetComponent<Player>().MyStack.Enqueue(card);       
    }

    public void AddToPlayer2CardStack(GameObject card)
    {
        player2SpawnPos.GetComponent<Player>().MyStack.Enqueue(card);
    }

    public void OnGameOver()
    {

        //short type of gameover.
        StopAllCoroutines();

        int player1cards = TurnBasedSystem.Instance.Players[0].Player.GetComponent<Player>().NoOfCards;
        int player2cards = TurnBasedSystem.Instance.Players[1].Player.GetComponent<Player>().NoOfCards;

        if(player1cards > player2cards)
        {
            GameOverText.text = "Player 1 Wins!";
            GameOverText.gameObject.SetActive(true);
        }
        else
        {
            GameOverText.text = "Player 2 Wins";
            GameOverText.gameObject.SetActive(true);
        }

        TurnBasedSystem.Instance.Players.Clear();
    }

}
