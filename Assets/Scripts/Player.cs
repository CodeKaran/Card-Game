﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class Player : PlayerProperties
{
    public static Player Instance;

    public TurnBasedSystem _TurnBasedSystem;
    public TurnBased _TurnBased;
    public Button ShowCardButton;

    private void Awake()
    {
        Instance = this;
    }

    void Start ()
    {
        //ShowCardButton.onClick.AddListener(delegate { DrawACard(); });

        _TurnBasedSystem = GameObject.Find("_GameManager").GetComponent<TurnBasedSystem>();

        foreach(TurnBased turnBased in _TurnBasedSystem.Players)
        {
            if (turnBased.Player.name == gameObject.name)
                _TurnBased = turnBased;
        }
	}
	
	void Update ()
    {
        isTurn = _TurnBased.isTurn;

        if (isTurn)
        {
            if(GameManager.Instance.currentMode == GameManager.Mode.None)
                ShowCardButton.interactable = true;           
        }
        else
        {
                ShowCardButton.interactable = false;
        }

        foreach(GameObject GO in MyStack)
        {
            GO.GetComponent<Cards>().numberText.enabled = false;
        }


        NoOfCards = MyStack.Count;
        cardsText.text = NoOfCards.ToString();


        if (MyStack.Count == 0)
        {
            //call gameover
            if(isTurn && MyStack.Count == 0)
                GameManager.Instance.OnGameOver();
        }

    }

    public void OnClickShowCard()
    {
        activeCard = DrawACard();
        GameManager.Instance.CardsOnTable.Add(activeCard);
        _TurnBased.activeCard = activeCard;
    }

    public GameObject DrawACard()
    {
        isTurn = false;
        _TurnBased.isTurn = isTurn;
        _TurnBased.wasPrevTurn = true;

        GameObject card = MyStack.Dequeue();

        card.transform.position = ShowCardPos.position;
        card.GetComponent<Cards>().numberText.enabled = true;
        GameManager.Instance.currentMode = GameManager.Mode.None;
        return card;       
    }
}