﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TurnBasedSystem : MonoBehaviour
{
    public static TurnBasedSystem Instance;

    public List<TurnBased> Players;

    private void Awake()
    {
        Instance = this;
    }

    private void Start()
    {
        ResetTurns();
    }

    private void Update()
    {
        UpdateTurns();
    }

    void ResetTurns()
    {
        for(int i = 0; i < Players.Count; i++)
        {
            if(i==0)
            {
                Players[i].isTurn = true;
                Players[i].wasPrevTurn = false;
            }
            else
            {
                Players[i].isTurn = false;
                Players[i].wasPrevTurn = false;
            }
        }
    }

    void UpdateTurns()
    {
        for(int i = 0; i < Players.Count; i++)
        {
            if (!Players[i].wasPrevTurn)
            {
                Players[i].isTurn = true;
                break;
            }
            else if (i == Players.Count - 1 && Players[i].wasPrevTurn)
            {
                if (GameManager.Instance.currentMode == GameManager.Mode.Battle || GameManager.Instance.currentMode == GameManager.Mode.War)
                    StartCoroutine(Check4Winner());

                if(GameManager.Instance.currentMode == GameManager.Mode.None)
                {
                    ResetTurns();
                }
            }
        }
    }   

    public IEnumerator Check4Winner()
    {
        yield return new WaitForSeconds(2f);

        for(int i = 0; i < Players.Count-1; i++)
        {
            if(Players[i].activeCard.GetComponent<Cards>().cardNumber > Players[i + 1].activeCard.GetComponent<Cards>().cardNumber)
            {               
                Players[i].Player.GetComponent<Player>().MyStack.Enqueue(Players[i].activeCard);
                Players[i].Player.GetComponent<Player>().MyStack.Enqueue(Players[i + 1].activeCard);

                //place the cards back to winner's deck 
                Players[i].activeCard.transform.position = Players[i].Player.transform.position;
                Players[i + 1].activeCard.transform.position = Players[i].Player.transform.position;

                if(GameManager.Instance.previousMode == GameManager.Mode.War)
                {
                    int totalcards = GameManager.Instance.FlipedCards.Count;

                    for (int n = 0; n < totalcards; n++)
                    {
                        GameObject card = GameManager.Instance.FlipedCards.Dequeue();
                        card.transform.position = Players[i].Player.transform.position;
                        Players[i].Player.GetComponent<Player>().MyStack.Enqueue(card);

                        if(n == totalcards - 1)
                        {
                            GameManager.Instance.previousMode = GameManager.Mode.None;
                        }
                    }
                }

                GameManager.Instance.currentMode = GameManager.Mode.None;
                GameManager.Instance.CardsOnTable.Clear();
                ResetTurns();
                StopAllCoroutines();
                break;
            }
            else if(Players[i].activeCard.GetComponent<Cards>().cardNumber < Players[i + 1].activeCard.GetComponent<Cards>().cardNumber)
            {
                Players[i+1].Player.GetComponent<Player>().MyStack.Enqueue(Players[i].activeCard);
                Players[i+1].Player.GetComponent<Player>().MyStack.Enqueue(Players[i + 1].activeCard);

                //place the cards back to winner's deck
                Players[i].activeCard.transform.position = Players[i+1].Player.transform.position;
                Players[i + 1].activeCard.transform.position = Players[i+1].Player.transform.position;


                if (GameManager.Instance.previousMode == GameManager.Mode.War)
                {
                    int totalcards = GameManager.Instance.FlipedCards.Count;

                    for (int n = 0; n < totalcards; n++)
                    {
                        GameObject card = GameManager.Instance.FlipedCards.Dequeue();
                        card.transform.position = Players[i + 1].Player.transform.position;
                        Players[i + 1].Player.GetComponent<Player>().MyStack.Enqueue(card);

                        if (n == totalcards - 1)
                        {
                            GameManager.Instance.previousMode = GameManager.Mode.None;
                        }
                    }
                }

                GameManager.Instance.currentMode = GameManager.Mode.None;
                GameManager.Instance.CardsOnTable.Clear();
                ResetTurns();
                StopAllCoroutines();
                break;
            }
            else if(GameManager.Instance.currentMode == GameManager.Mode.War && Players[i].activeCard.GetComponent<Cards>().cardNumber == Players[i + 1].activeCard.GetComponent<Cards>().cardNumber)
            {
                Players[i].activeCard.transform.position = Players[i].Player.GetComponent<Player>().WarCardPos.position;
                Players[i + 1].activeCard.transform.position = Players[i+1].Player.GetComponent<Player>().WarCardPos.position;

                GameManager.Instance.FlipedCards.Enqueue(Players[i].activeCard);
                GameManager.Instance.FlipedCards.Enqueue(Players[i+1].activeCard);

                GameManager.Instance.CardsOnTable.Clear();
                StartCoroutine(WarMode(Players[i].activeCard.GetComponent<Cards>().cardNumber));
            }
        }
    }

    public IEnumerator WarMode(int duelNumber)
    {
        Debug.Log(duelNumber);
        for (int i = 0; i < Players.Count - 1; i++)
        {
            for (int j = 0; j < duelNumber - 1; j++)
            {
                GameObject flipCard1 = Players[i].Player.GetComponent<Player>().MyStack.Dequeue();
                flipCard1.transform.position = Players[i].Player.GetComponent<Player>().CardFlipPos.position;
                GameManager.Instance.FlipedCards.Enqueue(flipCard1);

                GameObject flipCard2 = Players[i + 1].Player.GetComponent<Player>().MyStack.Dequeue();
                flipCard2.transform.position = Players[i+1].Player.GetComponent<Player>().CardFlipPos.position;
                GameManager.Instance.FlipedCards.Enqueue(flipCard2);

                yield return new WaitForSeconds(0.2f);

                GameManager.Instance.previousMode = GameManager.Mode.War;

                //break;
            }
        }

        ResetTurns();
        StopAllCoroutines();
    }
}

[System.Serializable]
public class TurnBased
{
    public GameObject Player;
    public GameObject activeCard;
    public bool isTurn = false;
    public bool wasPrevTurn = false;
}